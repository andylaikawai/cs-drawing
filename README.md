# Console Drawing Program
the program should work as follows:
 1. Create a new canvas
 2. Start drawing on the canvas by issuing various commands
 3. Quit

```
Command 		Description
C w h           Should create a new canvas of width w and height h.
L x1 y1 x2 y2   Should create a new line from (x1,y1) to (x2,y2). Currently only
                horizontal or vertical lines are supported. Horizontal and vertical lines
                will be drawn using the 'x' character.
R x1 y1 x2 y2   Should create a new rectangle, whose upper left corner is (x1,y1) and
                lower right corner is (x2,y2). Horizontal and vertical lines will be drawn
                using the 'x' character.
B x y c         Should fill the entire area connected to (x,y) with "colour" c. The
                behavior of this is the same as that of the "bucket fill" tool in paint
                programs.
Q               Should quit the program.
U               Undo the last command and revert to last drawn canvas
```

## Getting started
1. Install node.js(v.14.15.5) and npm(v6.14.11) via https://nodejs.org/en/
2. Run ```npm install``` in project root
3. Run ```npm start``` to start the drawing program

## Assumptions
#### Input
* There is no difference between uppercase and lowercase for input commands. Both are accepted
* Width and height of Canvas (w, h) must be an integer
* There is no limit on width and height of the canvas. However the screen can potentially overflow depending on user's screen size
* The coordinates (x1, y1, x2, y2) of lines (L) or rectangles (R) must be within the Canvas
* "Colour" (c) can be single character only
* Any extra input parameter (e.g. C 1 2 3) will be ignored 

#### Commands
* Creating a new canvas will overwrite the last created canvas (if any)
* When filling colour, if the target is a line ("x"), the command will replace the line with color 

## Sample I/O
Below is a sample run of the program. User input is prefixed with enter command:

```
enter command: C 20 4
----------------------
|                    |
|                    |
|                    |
|                    |
----------------------
```
```
enter command: L 1 2 6 2
----------------------
|                    |
|xxxxxx              |
|                    |
|                    |
----------------------
```
```
enter command: L 6 3 6 4
----------------------
|                    |
|xxxxxx              |
|     x              |
|     x              |
----------------------
```
```
enter command: R 14 1 18 3
----------------------
|             xxxxx  |
|xxxxxx       x   x  |
|     x       xxxxx  |
|     x              |
----------------------
```
```
enter command: B 10 3 o
----------------------
|oooooooooooooxxxxxoo|
|xxxxxxooooooox   xoo|
|     xoooooooxxxxxoo|
|     xoooooooooooooo|
----------------------
```
```
enter command: Q
```