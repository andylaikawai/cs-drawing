import {isOutOfBound} from "./utils.js";


const boundaryValidation = (canvas, node) => {
  if (isOutOfBound(canvas, node)) {
    throw new Error("You cannot draw outside the canvas");
  }
};

export const createCanvasInputValidation = (width, height) => {
  if (width <= 0) {
    throw new Error(`Canvas width should be > 1, received: ${width}`);
  }
  if (height <= 0) {
    throw new Error(`Canvas height should be > 1, received: ${height}`);
  }
};

export const drawLineInputValidation = (canvas, args) => {
  const [x1, y1, x2, y2] = args;
  boundaryValidation(canvas, {x: x1, y: y1});
  boundaryValidation(canvas, {x: x2, y: y2});

  if (x1 !== x2 && y1 !== y2) {
    throw new Error('You can only draw horizontal or vertical lines');
  }
};

export const fillCanvasInputValidation = (canvas, node, colour) => {
  boundaryValidation(canvas, node);
  if (!colour || colour.length > 1) {
    throw new Error('You can only fill with single character');
  }
};

export const undoInputValidation = (previousCanvas) => {
  if (!previousCanvas || previousCanvas.length < 1) {
    throw new Error('You have not create any canvas yet');
  }
};