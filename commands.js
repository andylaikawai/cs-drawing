import {colours} from "./config.js";
import {
  createCanvasInputValidation,
  drawLineInputValidation,
  fillCanvasInputValidation,
  undoInputValidation
} from './input-validation.js';
import {isOutOfBound} from "./utils.js";


const createCanvas = (args) => {
  const [width, height] = args.map(arg => parseInt(arg));
  createCanvasInputValidation(width, height);

  const row = new Array(width).fill(colours.EMPTY);
  return new Array(height).fill([...row]);
};

const drawHorizontalLine = (canvas, x1, x2, y) => {
  return canvas.map((row, rowIdx) => {
    if (rowIdx + 1 === y) {
      return row.map((cell, colIdx) => colIdx + 1 >= x1 && colIdx + 1 <= x2 ? colours.LINE : cell);
    }
    return row;
  });
};

const drawVerticalLine = (canvas, x, y1, y2) => {
  return canvas.map((row, rowIdx) => {
    if (rowIdx + 1 >= y1 && rowIdx + 1 <= y2) {
      return row.map((cell, colIdx) => colIdx + 1 === x ? colours.LINE : cell);
    }
    return row;
  });
};

const drawLine = (canvas, args) => {
  const nodes = args.map(arg => parseInt(arg));
  const [x1, y1, x2, y2] = nodes;
  drawLineInputValidation(canvas, nodes);

  if (x1 > x2) {
    const [x1, x2] = [x2, x1];
  }
  if (y1 > y2) {
    const [y1, y2] = [y2, y1];
  }

  if (x1 !== x2) {
    return drawHorizontalLine(canvas, x1, x2, y1);
  }
  return drawVerticalLine(canvas, x1, y1, y2);
};

const drawRect = (canvas, args) => {
  const [x1, y1, x2, y2] = args;
  const withTopLine = drawLine(canvas, [x1, y1, x2, y1]);
  const withBottomLine = drawLine(withTopLine, [x1, y2, x2, y2]);
  const withLeftLine = drawLine(withBottomLine, [x1, y1, x1, y2]);
  return drawLine(withLeftLine, [x2, y1, x2, y2]);

};

const fillCanvas = (canvas, args) => {
  const [xString, yString, colour] = args;
  const [x, y] = [xString, yString].map(arg => parseInt(arg));
  fillCanvasInputValidation(canvas, {x, y}, colour);
  return floodFill(canvas, {x, y}, canvas[y - 1][x - 1], colour);
};

const floodFill = (canvas, node, targetColor, replacementColor) => {
  const {x, y} = node;
  if (isOutOfBound(canvas, node)
    || canvas[y - 1][x - 1] === replacementColor
    || canvas[y - 1][x - 1] !== targetColor
  ) {
    return canvas;
  }

  const coloredCanvas = canvas.map((row, rowIdx) => {
    if (rowIdx + 1 === y) {
      return row.map((col, colIdx) => colIdx + 1 === x ? replacementColor : col);
    }
    return row;
  });
  const northFilled = floodFill(coloredCanvas, {x, y: y - 1}, targetColor, replacementColor);
  const southFilled = floodFill(northFilled, {x, y: y + 1}, targetColor, replacementColor);
  const westFilled = floodFill(southFilled, {x: x - 1, y}, targetColor, replacementColor);
  return floodFill(westFilled, {x: x + 1, y}, targetColor, replacementColor);
};

const undo = (previousCanvas) => {
  undoInputValidation(previousCanvas);
  const currentCanvas = previousCanvas.pop();
  const lastCanvas = previousCanvas.length ? previousCanvas.pop() : null;
  return {
    lastCanvas,
    newPreviousCanvas: previousCanvas
  }
};

export {
  createCanvas,
  drawLine,
  drawRect,
  fillCanvas,
  undo
}