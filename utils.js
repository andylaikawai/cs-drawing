export const getCanvasWidth = (canvas) => canvas[0].length;
export const getCanvasHeight = (canvas) => canvas.length;

export const isOutOfBound = (canvas, node) => {
  const {x, y} = node;
  const width = getCanvasWidth(canvas);
  const height = getCanvasHeight(canvas);
  return (
    [x, y].some(coordinate => coordinate < 1)
    || x > width
    || y > height
  );
};

export const getComposedRegex = (regexList) => new RegExp(regexList.map(regex => regex.source).join("|"));
