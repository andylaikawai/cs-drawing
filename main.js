import {promptMessage} from './config.js';
import prompt from 'prompt';
import requestInput from "./command-handler.js";


const main = () => {
  prompt.message = promptMessage;
  prompt.start();
  return requestInput();
};

export default main;