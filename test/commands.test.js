import assert from 'assert';
import * as commands from '../commands.js';
import {colours} from '../config.js';

const emptyCanvas = [
  [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
  [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
  [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
  [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
  [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
];
const outOfBoundError = `You cannot draw outside the canvas`;
const diagonalLineError = `You can only draw horizontal or vertical lines`;
const multipleCharColourError = 'You can only fill with single character';

describe('Commands', () => {
  describe('create canvas command', () => {
    describe('handle input error', () => {
      it('throw error if width < 1', () => {
        const invalidWidthError = `Canvas width should be > 1, received: 0`;
        assert.throws(() => commands.createCanvas(['0', '1']), Error, invalidWidthError);
      });

      it('throw error if height < 1', () => {
        const invalidHeightError = `Canvas height should be > 1, received: 0`;
        assert.throws(() => commands.createCanvas(['1', '0']), Error, invalidHeightError);
      });
    });

    describe('create a canvas represented by an 2D array', () => {
      it('create a 4 by 5 canvas filled with empty cells', () => {
        const result = commands.createCanvas([4, 5]);
        assert.deepEqual(emptyCanvas, result);
      });
    });
  });

  describe('draw line command', () => {
    describe('handle invalid input', () => {
      it('throw error if x1 is out of bound', () => {
        assert.throws(() => commands.drawLine(emptyCanvas, ['5', '1', '1', '1']), Error, outOfBoundError);
      });

      it('throw error if y1 is out of bound', () => {
        assert.throws(() => commands.drawLine(emptyCanvas, ['1', '6', '1', '1']), Error, outOfBoundError);
      });

      it('throw error if x2 is out of bound', () => {
        assert.throws(() => commands.drawLine(emptyCanvas, ['1', '1', '5', '1']), Error, outOfBoundError);
      });

      it('throw error if y2 is out of bound', () => {
        assert.throws(() => commands.drawLine(emptyCanvas, ['1', '1', '1', '6']), Error, outOfBoundError);
      });

      it('throw error if line to be drawn is diagonal', () => {
        assert.throws(() => commands.drawLine(emptyCanvas, ['1', '2', '3', '4']), Error, diagonalLineError);
      });
    });

    describe('draw line', () => {
      it('draw horizontal line', () => {
        const expected = [
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.LINE, colours.LINE, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
        ];

        const result = commands.drawLine(emptyCanvas, ['1', '2', '3', '2']);
        assert.deepEqual(result, expected);
      });

      it('draw overlapping horizontal line on non-empty canvas', () => {
        const canvas = [
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.LINE, colours.LINE, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
        ];

        const expected = [
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.LINE, colours.LINE, colours.LINE, colours.LINE],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
        ];

        const result = commands.drawLine(canvas, ['3', '2', '4', '2']);
        assert.deepEqual(result, expected);
      });

      it('draw vertical line', () => {
        const expected = [
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
        ];

        const result = commands.drawLine(emptyCanvas, ['3', '1', '3', '3']);
        assert.deepEqual(result, expected);
      });

      it('draw overlapping vertical line on non-empty canvas', () => {
        const canvas = [
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
        ];
        const expected = [
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
        ];
        const result = commands.drawLine(canvas, ['3', '2', '3', '5']);
        assert.deepEqual(result, expected);
      });
    });
  });

  describe('draw rectangle command', () => {
    describe('handle invalid input', () => {
      it('throw error if x1 is out of bound', () => {
        assert.throws(() => commands.drawRect(emptyCanvas, ['5', '1', '1', '1']), Error, outOfBoundError);
      });

      it('throw error if y1 is out of bound', () => {
        assert.throws(() => commands.drawRect(emptyCanvas, ['1', '6', '1', '1']), Error, outOfBoundError);
      });

      it('throw error if x2 is out of bound', () => {
        assert.throws(() => commands.drawRect(emptyCanvas, ['1', '1', '5', '1']), Error, outOfBoundError);
      });

      it('throw error if y2 is out of bound', () => {
        assert.throws(() => commands.drawRect(emptyCanvas, ['1', '1', '1', '6']), Error, outOfBoundError);
      });
    });

    describe('draw rectangle', () => {
      it('draw rectangle on empty canvas', () => {
        const expected = [
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.LINE, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.LINE, colours.LINE],
        ];
        const result = commands.drawRect(emptyCanvas, ['2', '2', '4', '5']);
        assert.deepEqual(result, expected);
      });

      it('draw overlapping rectangle on non-empty canvas', () => {
        const canvas = [
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.LINE, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.LINE, colours.LINE],
        ];
        const expected = [
          [colours.LINE, colours.LINE, colours.LINE, colours.EMPTY],
          [colours.LINE, colours.LINE, colours.LINE, colours.LINE],
          [colours.LINE, colours.LINE, colours.LINE, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.LINE, colours.LINE],
        ];
        const result = commands.drawRect(canvas, ['1', '1', '3', '3']);
        assert.deepEqual(result, expected);
      });
    });
  });

  describe('fill canvas command', () => {
    describe('handle invalid input', () => {
      it('throw error if x is out of bound', () => {
        assert.throws(() => commands.fillCanvas(emptyCanvas, ['5', '1', 'o']), Error, outOfBoundError);
      });

      it('throw error if y is out of bound', () => {
        assert.throws(() => commands.fillCanvas(emptyCanvas, ['1', '6', 'o']), Error, outOfBoundError);
      });

      it('throw error if color contain multiple characters', () => {
        assert.throws(() => commands.fillCanvas(emptyCanvas, ['1', '1', 'xx']), Error, multipleCharColourError);
      });
    });

    describe('fill canvas', () => {
      it('fill empty canvas', () => {
        const colour = 'o';
        const expected = [
          [colour, colour, colour, colour],
          [colour, colour, colour, colour],
          [colour, colour, colour, colour],
          [colour, colour, colour, colour],
          [colour, colour, colour, colour],
        ];

        const result = commands.fillCanvas(emptyCanvas, ['1', '1', colour]);
        assert.deepEqual(result, expected);
      });

      it('fill canvas within a rectangle boundary', () => {
        const colour = 'o';
        const canvas = [
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.LINE, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.LINE, colours.LINE],
        ];
        const expected = [
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.LINE, colours.LINE],
          [colours.EMPTY, colours.LINE, colour, colours.LINE],
          [colours.EMPTY, colours.LINE, colour, colours.LINE],
          [colours.EMPTY, colours.LINE, colours.LINE, colours.LINE],
        ];

        const result = commands.fillCanvas(canvas, ['3', '3', colour]);
        assert.deepEqual(result, expected);
      });

      it('fill canvas within a single line boundary', () => {
        const colour = 'o';
        const canvas = [
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
        ];
        const expected = [
          [colour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colour, colours.LINE, colours.EMPTY, colours.EMPTY],
        ];

        const result = commands.fillCanvas(canvas, ['1', '4', colour]);
        assert.deepEqual(result, expected);
      });

      it('fill canvas within multiple line boundaries', () => {
        const colour = 'o';
        const canvas = [
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.EMPTY],
          [colours.EMPTY, colours.EMPTY, colours.LINE, colours.LINE],
          [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
          [colours.LINE, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
        ];
        const expected = [
          [colour, colour, colours.LINE, colours.EMPTY],
          [colour, colour, colours.LINE, colours.LINE],
          [colour, colour, colour, colour],
          [colours.LINE, colours.LINE, colour, colour],
          [colours.EMPTY, colours.LINE, colour, colour],
        ];

        const result = commands.fillCanvas(canvas, ['2', '2', colour]);
        assert.deepEqual(result, expected);
      });

      it('fill canvas with line as a target', () => {
        const colour = 'o';
        const canvas = [
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colours.LINE, colours.EMPTY, colours.EMPTY],
        ];
        const expected = [
          [colours.EMPTY, colour, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colour, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colour, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colour, colours.EMPTY, colours.EMPTY],
          [colours.EMPTY, colour, colours.EMPTY, colours.EMPTY],
        ];

        const result = commands.fillCanvas(canvas, ['2', '2', colour]);
        assert.deepEqual(result, expected);
      });

      it('fill canvas with custom color as target', () => {
        const originalColour = 'o';
        const newColour = ']';
        const canvas = [
          [originalColour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [originalColour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [originalColour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [originalColour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [originalColour, colours.LINE, colours.EMPTY, colours.EMPTY],
        ];

        const expected = [
          [newColour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [newColour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [newColour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [newColour, colours.LINE, colours.EMPTY, colours.EMPTY],
          [newColour, colours.LINE, colours.EMPTY, colours.EMPTY],
        ];

        const result = commands.fillCanvas(canvas, ['1', '2', newColour]);
        assert.deepEqual(result, expected);
      });
    });
  });

  describe('Undo command', () => {
    it('undo the last operation', () => {
      const canvas1 = [
        [colours.EMPTY]
      ];
      const canvas2 = [
        [colours.EMPTY, colours.EMPTY]
      ];
      const previousCanvas = [canvas1, canvas2];
      const result = commands.undo(previousCanvas);

      assert(result, canvas2);
    });
  });
});