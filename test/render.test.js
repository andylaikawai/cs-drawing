import assert from 'assert';
import sinon from 'sinon';
import render from '../render.js';
import {colours} from '../config.js';

const canvas = [
  [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
  [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
  [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
  [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
  [colours.EMPTY, colours.EMPTY, colours.EMPTY, colours.EMPTY],
];

describe('Render', () => {
  describe('print a canvas surrounded by borders', () => {
    it('print the canvas to console', () => {
      const expectedCanvasString =
        "------\n" +
        "|    |\n" +
        "|    |\n" +
        "|    |\n" +
        "|    |\n" +
        "|    |\n" +
        "------";
      const spy = sinon.spy(console, 'log');
      render(canvas);
      assert(spy.calledWith(expectedCanvasString));
    });
  });
});