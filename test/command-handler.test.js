import assert from 'assert';
import sinon from 'sinon';
import {processCommand} from '../command-handler.js';
import {colours} from '../config.js';

const canvas = [
  [colours.EMPTY],
];
const noCanvasError = `You must first create a Canvas`;
const previousCanvas = [canvas];

describe('Command Handler', () => {
  describe('throw error for invalid input', () => {
    it('must create canvas before draw line', () => {
      const spy = sinon.spy(() => canvas);
      const caller = () => processCommand('L 1 2 3 4', null, {drawLine: spy}, previousCanvas);
      assert.throws(caller, Error, noCanvasError);
    });

    it('must create canvas before draw rectangle', () => {
      const spy = sinon.spy(() => canvas);
      const caller = () => processCommand('R 1 2 3 4', null, {drawRect: spy}, previousCanvas);
      assert.throws(caller, Error, noCanvasError);
    });

    it('must create canvas before fill canvas', () => {
      const spy = sinon.spy(() => canvas);
      const caller = () => processCommand('B 1 2 o', null, {fillCanvas: spy}, previousCanvas);
      assert.throws(caller, Error, noCanvasError);
    });
  });

  describe('Handle accepted command from console input', () => {
    it('Handle create canvas command', () => {
      const spy = sinon.spy(() => canvas);
      processCommand('C 5 5', null, {createCanvas: spy}, previousCanvas);
      assert(spy.calledWith(['5', '5']));
    });

    it('Handle draw line command', () => {
      const spy = sinon.spy(() => canvas);
      processCommand('L 1 2 3 4', canvas, {drawLine: spy}, previousCanvas);
      assert(spy.calledWith(canvas, ['1', '2', '3', '4']));
    });

    it('Handle draw rect command', () => {
      const spy = sinon.spy(() => canvas);
      processCommand('R 1 2 3 4', canvas, {drawRect: spy}, previousCanvas);
      assert(spy.calledWith(canvas, ['1', '2', '3', '4']));
    });

    it('Handle fill canvas command', () => {
      const spy = sinon.spy(() => canvas);
      processCommand('B 1 2 o', canvas, {fillCanvas: spy}, previousCanvas);
      assert(spy.calledWith(canvas, ['1', '2', 'o']));
    });

    it('Handle undo command', () => {
      const spy = sinon.spy(() => canvas);
      processCommand('U', canvas, {undo: spy}, previousCanvas);
      assert(spy.calledWith(previousCanvas));
    });
  });
});