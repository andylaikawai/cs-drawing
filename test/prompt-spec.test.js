import {stdin} from 'mock-stdin'
import assert from 'assert';
import sinon from 'sinon';
import main from '../main.js';

let io = null;

// Key codes
const keys = {
  enter: '\x0D',
  space: '\x20'
};

const sendKeystrokes = async () => {
  io.send(["C 1 1"]);
  io.send(keys.enter)
};

describe('prompt integration test', () => {
  it('create canvas and print it', async () => {
    io = stdin();

    const spy = sinon.spy(console, 'log');

    main();
    await sendKeystrokes();

    const expectedCanvasString =
      "---\n" +
      "|  |\n" +
      "---";

    assert(spy.calledWith(expectedCanvasString));
    io.restore();
  });
});
