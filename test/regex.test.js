import assert from 'assert';
import {allCommandsRegex} from '../config.js';


describe('Regex tester', () => {
  describe('match input regex', () => {
    it('match create canvas command', () => {
      const input = 'C 1 2';
      assert.match(input, allCommandsRegex);
    });

    it('does not match create canvas command', () => {
      const input = 'C 1.1 2';
      assert.doesNotMatch(input, allCommandsRegex);
    });
  });
});