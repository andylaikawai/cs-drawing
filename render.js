import {colours} from "./config.js";
import {getCanvasWidth} from "./utils.js";

const canvasArrayToString = (canvas) => {
  const cellSeparator = '';
  const rowSeparator = "\n";
  return canvas.map(row => row.join(cellSeparator)).join(rowSeparator);
};

const render = (canvas) => {
  const width = getCanvasWidth(canvas);
  const borderWidth = width + colours.LEFT_BORDER.length + colours.RIGHT_BORDER.length;
  const topBorder = new Array(borderWidth).fill(colours.TOP_BORDER);
  const bottomBorder = new Array(borderWidth).fill(colours.BOTTOM_BORDER);
  const canvasWithLeftRightBorder = canvas.map(row => [
    colours.LEFT_BORDER,
    ...row,
    colours.RIGHT_BORDER
  ]);

  const canvasWithBorder = [
    topBorder,
    ...canvasWithLeftRightBorder,
    bottomBorder
  ];

  const canvasString = canvasArrayToString(canvasWithBorder);
  console.log(canvasString);
};

export default render;