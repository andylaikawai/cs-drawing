import {getComposedRegex} from "./utils.js";

const description = `
command 		description
c w h           should create a new canvas of width w and height h.
l x1 y1 x2 y2   should create a new line from (x1,y1) to (x2,y2). currently only
                horizontal or vertical lines are supported. horizontal and vertical lines
                will be drawn using the 'x' character.
r x1 y1 x2 y2   should create a new rectangle, whose upper left corner is (x1,y1) and
                lower right corner is (x2,y2). horizontal and vertical lines will be drawn
                using the 'x' character.
b x y c         should fill the entire area connected to (x,y) with "colour" c. the
                behavior of this is the same as that of the "bucket fill" tool in paint
                programs.
q               should quit the program.
`;

const actions = {
  CREATE_CANVAS: 'C',
  DRAW_LINE: 'L',
  DRAW_RECT: 'R',
  FILL: 'B',
  QUIT: 'Q',
  UNDO : 'U'
};

const colours = {
  EMPTY: ' ',
  LINE: 'x',
  LEFT_BORDER: '|',
  RIGHT_BORDER: '|',
  TOP_BORDER: '-',
  BOTTOM_BORDER: '-',
};


const createCanvasRegex = /[cC]\s\d+\s\d+/;
const drawLineRegex = /[lL]\s\d+\s\d+\s\d+\s\d+/;
const drawRectRegex = /[rR]\s\d+\s\d+\s\d+\s\d+/;
const fillRegex = /[bB]\s\d+\s\d+\s./;
const quitRegex = /[qQ]/;
const undoRegex = /[uU]/;


const allCommandsRegex = getComposedRegex([
  createCanvasRegex,
  drawLineRegex,
  drawRectRegex,
  fillRegex,
  quitRegex,
  undoRegex
]);

const schema = {
  properties: {
    command: {
      description: "enter command",
      pattern: allCommandsRegex,
      message: 'Invalid command received. See below for available commands\n' + description,
      required: true
    }
  }
};

const promptMessage = "";

export {
  allCommandsRegex,
  promptMessage,
  schema,
  actions,
  colours
};