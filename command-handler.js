import prompt from 'prompt';
import {actions, schema} from "./config.js";
import * as commands from './commands.js';
import render from './render.js';

const onErr = (err) => {
  console.error(err);
  return 1;
};

const throwIfNoCanvas = (canvas) => {
  if (!canvas) {
    throw new Error("You must first create a Canvas");
  }
};

const processCommand = (command, canvas, commands, previousCanvas) => {
  const whiteSpace = /\s/;
  const args = command.split(whiteSpace);
  const [action, ...params] = args;

  let newCanvas;
  switch (action.toUpperCase()) {
    case actions.CREATE_CANVAS:
      newCanvas = commands.createCanvas(params);
      render(newCanvas);
      previousCanvas.push(newCanvas);
      requestInput(newCanvas, previousCanvas);
      return;
    case actions.DRAW_LINE:
      throwIfNoCanvas(canvas);
      newCanvas = commands.drawLine(canvas, params);
      render(newCanvas);
      previousCanvas.push(newCanvas);
      requestInput(newCanvas, previousCanvas);
      return;
    case actions.DRAW_RECT:
      throwIfNoCanvas(canvas);
      newCanvas = commands.drawRect(canvas, params);
      render(newCanvas);
      previousCanvas.push(newCanvas);
      requestInput(newCanvas, previousCanvas);
      return;
    case actions.FILL:
      throwIfNoCanvas(canvas);
      newCanvas = commands.fillCanvas(canvas, params);
      render(newCanvas);
      previousCanvas.push(newCanvas);
      requestInput(newCanvas, previousCanvas);
      return;
    case actions.QUIT:
      return;
    case actions.UNDO:
      const {
        lastCanvas,
        newPreviousCanvas
      } = commands.undo(previousCanvas);

      if (lastCanvas) {
        render(lastCanvas);
      }
      requestInput(lastCanvas, newPreviousCanvas);
      return;
    default:
      throw new Error(`Unknown command received: ${action}`);
  }

};

const handleInput = (err, {command}, canvas, commands, previousCanvas) => {
  if (err) {
    return onErr(err);
  }
  try {
    processCommand(command, canvas, commands, previousCanvas);
  } catch (e) {
    console.error("ERROR:", e.message);
    requestInput(canvas, previousCanvas);
  }
};

const requestInput = async (canvas = null, previousCanvas = []) => {
  prompt.get(schema, (err, input) => handleInput(err, input, canvas, commands, previousCanvas));
};

export {processCommand};
export default requestInput;